<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFingerprintingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fingerprinting', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip')->default(NULL);
            $table->string('port')->default(NULL);
            $table->string('vlan')->default(NULL);
            $table->string('cpe_mac')->default(NULL);
            $table->string('device_mac')->default(NULL);
            $table->string('model')->default(NULL);
            $table->string('original_uri')->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fingerprinting');
    }
}