<!DOCTYPE html>
<html lang="en">

<head>
    <title>pass-through</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script type="text/javascript">

        var model            = getIPhoneModel()
        var width            = window.screen.width
        var height           = window.screen.height
        var devicePixelRatio = window.devicePixelRatio

        function getIPhoneModel() {
            // Create a canvas element which can be used to retrieve information about the GPU.
            var canvas = document.createElement("canvas");
            if (canvas) {
                var context = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
                if (context) {
                    var info = context.getExtension("WEBGL_debug_renderer_info");
                    if (info) {
                        var renderer = context.getParameter(info.UNMASKED_RENDERER_WEBGL);
                    }
                }
            }

            // iPhone X
            if ((height / width == 812 / 375) && (devicePixelRatio == 3)) {
                return "iPhone X";
            } else if ((height / width == 896 / 414) && (devicePixelRatio == 3)) {
                return "iPhone XS Max";
            } else if ((height / width == 896 / 414) && (devicePixelRatio == 2)) {
                return "iPhone XR";
            } else if ((height / width == 1024 / 768) && (devicePixelRatio == 2)) {
                return "iPad 4";
            }
            else if ((height / width == 736 / 414) && (devicePixelRatio == 3)) {
                switch (renderer) {
                    default:
                    return "iPhone 6 Plus, 6s Plus, 7 Plus or 8 Plus";
                    case "Apple A8 GPU":
                    return "iPhone 6 Plus";
                    case "Apple A9 GPU":
                    return "iPhone 6s Plus";
                    case "Apple A10 GPU":
                    return "iPhone 7 Plus";
                    case "Apple A11 GPU":
                    return "iPhone 8 Plus";
                }
                // iPhone 6+/6s+/7+ and 8+ in zoom mode
            } else if ((height / width == 667 / 375) && (devicePixelRatio == 3)) {
                switch(renderer) {
                    default:
                    return "iPhone 6 Plus, 6s Plus, 7 Plus or 8 Plus (display zoom)";
                    case "Apple A8 GPU":
                    return "iPhone 6 Plus (display zoom)";
                    case "Apple A9 GPU":
                    return "iPhone 6s Plus (display zoom)";
                    case "Apple A10 GPU":
                    return "iPhone 7 Plus (display zoom)";
                    case "Apple A11 GPU":
                    return "iPhone 8 Plus (display zoom)";
                }
                // iPhone 6/6s/7 and 8
            } else if ((height / width == 667 / 375) && (devicePixelRatio == 2)) {
                switch(renderer) {
                    default:
                    return "iPhone 6, 6s, 7 or 8";
                    case "Apple A8 GPU":
                    return "iPhone 6";
                    case "Apple A9 GPU":
                    return "iPhone 6s";
                    case "Apple A10 GPU":
                    return "iPhone 7";
                    case "Apple A11 GPU":
                    return "iPhone 8";
                }
                // iPhone 5/5C/5s/SE or 6/6s/7 and 8 in zoom mode
            } else if ((height / width == 1.775) && (devicePixelRatio == 2)) {
                switch(renderer) {
                    default:
                    return "iPhone 5, 5C, 5S, SE or 6, 6s, 7 and 8 (display zoom)";
                    case "PowerVR SGX 543":
                    return "iPhone 5 or 5c";
                    case "Apple A7 GPU":
                    return "iPhone 5s";
                    case "Apple A8 GPU":
                    return "iPhone 6 (display zoom)";
                    case "Apple A9 GPU":
                    return "iPhone SE or 6s (display zoom)";
                    case "Apple A10 GPU":
                    return "iPhone 7 (display zoom)";
                    case "Apple A11 GPU":
                    return "iPhone 8 (display zoom)";
                }
                // iPhone 4/4s
            } else if ((height / width == 1.5) && (devicePixelRatio == 2)) {
                switch(renderer) {
                    default:
                    return "iPhone 4 or 4s";
                    case "PowerVR SGX 535":
                    return "iPhone 4";
                    case "PowerVR SGX 543":
                    return "iPhone 4s";
                }
                // iPhone 1/3G/3GS
            } else if ((height / width == 1.5) && (devicePixelRatio == 1)) {
                switch(renderer) {
                    default:
                    return "iPhone 1, 3G or 3GS";
                    case "ALP0298C05":
                    return "iPhone 3GS";
                    case "S5L8900":
                    return "iPhone 1, 3G";
                }
            } else {
                return "Not an iPhone";
            }
        }


        console.log(model,width,height,devicePixelRatio);

        var currentUrl = window.location.href;
        var newUrl = currentUrl.replace("fingerprinting", "fingerprinting/tasks");

        // alert(newUrl);

        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: "{{ $APP_URL }}fingerprinting/store",
            data: {'cpe_mac' : '{!! $cpe_mac !!}', 'device_mac' : '{!! $device_mac !!}','original_uri':'{!! $original_uri !!}', 'model' : model, 'ip': '{!! $ip !!}', 'port': '{!! $port !!}', 'vlan': '{!! $vlan !!}'   }, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response){ // What to do if we succeed
                console.log(response);
                window.location.href = newUrl;
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });


    </script>

</body>

</html>



