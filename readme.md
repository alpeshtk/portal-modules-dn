#portal-modules


Framework : **Laravel 5.8**

https://laravel.com/docs/5.8/releases

Requirement

- Composer : https://getcomposer.org/download/
- PHP version 7+


To clone & start the project

    git clone ssh://git@hq-bitbucket1.benunets.com:7999/ssc/fingerprinting.git
    cd fingerprinting/
    composer install
    cat .env.example > .env
    php artisan key:generate
    chmod -R 777 storage/
    php artisan serve


You should see this on the console

![](https://i.imgur.com/VghwPdy.png)

Now, Go to : [http://127.0.0.1:8000/](http://127.0.0.1:8000/) , you should see

![](https://i.imgur.com/8lCDJBN.png)



#Docker 

Requirement

- Docker  : https://docs.docker.com/get-started/

To run this project in docker container.

	docker-compose up
	
You should see this on the console
	
![](https://i.imgur.com/jieTpRY.png)

Now, Go to : [http://0.0.0.0:8000/](http://0.0.0.0:8000/) , you should see
	
![](https://i.imgur.com/OWTk8Ml.png)
	