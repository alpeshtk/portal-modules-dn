<?php
namespace App\Http\Controllers;

use Redirect, Response;
use Illuminate\Support\Facades\Input;
use App\Fingerprinting;
use App\CURL;

class FingerPrintingController extends Controller {


    public function init() {

        if(Input::get('key') != 'NDN2M3ItYmVudS1uZXR3b3Jrcy01NUMtcDBydDQh') {
            return Redirect::to('/')->with('error', 'Key is not match');
        }

        $inputs       = Input::all();

        $ip           = $inputs['ip'];
        $port         = $inputs['port'];
        $vlan         = $inputs['vlan'];
        $cpe_mac      = $inputs['cpe_mac'];
        $device_mac   = $inputs['device_mac'];
        $original_uri = $inputs['original_uri'];

        $APP_URL = env('APP_URL');

        return view('iphone.index', get_defined_vars());
    }

    public function store() {

        $inputs       = Input::all();
        $ip           = $inputs['ip'];
        $port         = $inputs['port'];
        $vlan         = $inputs['vlan'];
        $cpe_mac      = $inputs['cpe_mac'];
        $device_mac   = $inputs['device_mac'];
        $original_uri = $inputs['original_uri'];
        $model        = $inputs['model'];

        $fingerprinting = Fingerprinting::where('device_mac',$device_mac)->where('cpe_mac',$cpe_mac)->first();

        if($fingerprinting == null) {
            $fingerprinting = new Fingerprinting;
        }

        $fingerprinting->ip           = $ip;
        $fingerprinting->port         = $port;
        $fingerprinting->vlan         = $vlan;
        $fingerprinting->cpe_mac      = $cpe_mac;
        $fingerprinting->device_mac   = $device_mac;
        $fingerprinting->model        = $model;
        $fingerprinting->original_uri = $original_uri;
        $fingerprinting->save();

        return Response::json($fingerprinting);

    }

    public function tasks() {

        $inputs       = Input::all();
        $ip           = $inputs['ip'];
        $port         = $inputs['port'];
        $vlan         = $inputs['vlan'];
        $cpe_mac      = $inputs['cpe_mac'];
        $device_mac   = $inputs['device_mac'];
        $original_uri = $inputs['original_uri'];

        $fingerprinting = Fingerprinting::where('device_mac',$device_mac)->where('cpe_mac',$cpe_mac)->first();
        $original_url   = $fingerprinting->original_uri;

        //Make a PUT
        $data                 = [];
        $data['device_model'] = $fingerprinting->model;
        $data                 = json_encode($data);
        $url                  = 'http://'.$ip.':'.$port.'/vse/vcpe/'.$cpe_mac.'/vlan/'.$vlan.'/device/'.$device_mac.'/fingerprint_update';
        $result               = CURL::post($url,$data);

        return Redirect::to(urldecode($original_url));
    }

}