FROM composer:1.8.5 as build_stage

COPY . /src
WORKDIR /src
RUN composer install

FROM php:7.2.17-fpm-alpine3.8

# Install dependencies
RUN apk --no-cache add \
php7-opcache \
php7-mbstring \
php7-session \
php7-openssl \
php7-tokenizer \
php7-json \
php7-pdo \
php7-pdo_pgsql \
php7-curl \
php7-pgsql

COPY --from=build_stage /src  /var/www
RUN ls -al
RUN cp /var/www/.env.example /var/www/.env
RUN cp /var/www/php-fpm/php.ini /usr/local/etc/php/
RUN cp /usr/lib/php7/modules/*.so /usr/local/lib/php/extensions/no-debug-non-zts-20170718

# Expose port 9000 and start php-fpm server
EXPOSE 9000

WORKDIR /var/www
RUN ls -al
RUN chmod -R 777 storage
RUN chmod +x run.sh
RUN cp run.sh /tmp

ENTRYPOINT ["/tmp/run.sh"]

CMD ["php-fpm"]